"""

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)


"""

__author__ = 'Murray Andrews'


# TODO: This is work in progress - not used yet


# ------------------------------------------------------------------------------
class GroanResult(object):
    """
    This is the standard result package returned by groan expect plugins.
    """

    # --------------------------------------------------------------------------
    def __init__(self, success, reason=None, params=None):
        """
        Create a new groan result.

        :param success:         Whether or not the check succeeded.
        :param reason:          A text message explaining what happened.
        :param params:          An optional dictionary of new parameters obtained
                                by the plugin from the test result. A value of
                                None (default) will be converted to an empty
                                dictionary.
        :type success:          bool
        :type reason:           str
        :type params:           dict[str, str]
        """

        self.success = success
        self.reason = reason
        self.params = params if params else {}
