"""
Groan expect plugin to match strings from stderr of the command.

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

from groan.expect import match

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def check(config, status, out_data, err_data):
    """
    Check that stderr matches a given pattern or *any* of a list of patterns.
    Matching is done by splitting the out_data into lines and matching each with
    re.search() so the searhing is not anchored.

    :param config:      The configuration for this check read from the YAML test spec.
    :param status:      The return status for the command.
    :param out_data:    The data the command wrote to stdout. Not used.
    :param err_data:    The data the command wrote to stderr.

    :type config:       T
    :type status:       int
    :type out_data:     str
    :type err_data:     str

    :return:            A tuple (success, fail reason) where success is True if the
                        test passed and false otherwise and the fail reason is a
                        string explaining the test result.
    :rtype:             (bool, str)

    :raise ValueError:  If the config is malforrmed.
    """

    # We cheat and let the normal match do all the work - just fake input.

    return match.check(config, status, err_data, err_data)
