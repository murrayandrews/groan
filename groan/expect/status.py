"""
Groan expect plugin to check return status of the command.

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

import re

__author__ = 'Murray Andrews'

RE_COMPARE_TO_NUM = r'\s*(?P<op>(=|==|>|<|>=|<=|not|\^|\^=|!|!=|<>))?\s*(?P<num>[+-]?\d+)\s*$'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def check(config, status, out_data, err_data):
    """
    Check return status of command. Can either be an integer or one of the
    following comparators. The entire string should be quoted in the YAML file
    for safety.

        nnn
        = nnn
        == nnn
        > nnn
        >= nnn
        < nnn
        <= nnn

    or one of the following variants for not-equal:
        not nnn
        ^ nnn
        ^= nnn
        <> nnn
        != nnn (Warning: ! is special to YAML and must be quoted)
        ! nnn (Warning: ! is special to YAML and must be quoted)

    :param config:      The configuration for this check read from the YAML test spec.
    :param status:      The return status for the command.
    :param out_data:    The data the command wrote to stdout.
    :param err_data:    The data the command wrote to stderr.

    :type config:       T
    :type status:       int
    :type out_data:     str
    :type err_data:     str

    :return:            A tuple (success, fail reason) where success is True if the
                        test passed and false otherwise and the fail reason is a
                        string explaining the test result.
    :rtype:             (bool, str)

    :raise ValueError:  If the config is malforrmed.
    """

    if isinstance(config, int):
        # Integer status must match exactly.
        result = status == config
        return result, (str(status) if result else 'Expected {}, got {}'.format(config, status))

    # -----------------------------
    # Not an integer status - check for a string containing a comparison operator
    # and an integer.

    if not isinstance(config, str):
        raise ValueError('Bad config - string expected: {}'.format(config))

    m = re.match(RE_COMPARE_TO_NUM, config)

    if not m:
        raise ValueError('Bad config - cannot decode: {}'.format(config))

    if m.group('op') in ('not', '!', '!=', '<>', '^', '^='):
        op = '!='
    elif not m.group('op') or m.group('op') in ('=', '=='):
        op = '=='
    else:
        op = m.group('op')

    value = int(m.group('num'))

    result = eval('{}{}{}'.format(status, op, value))

    return result, (str(status) if result else 'Expected {}, got {}'.format(config, status))
