"""
Groan expect plugin to match strings from stdout of the command.

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

import re

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def check(config, status, out_data, err_data):
    """
    Check that stdout matches a given pattern or *any* of a list of patterns.
    Matching is done by splitting the out_data into lines and matching each with
    re.search() so the searching is not anchored.

    :param config:      The configuration for this check read from the YAML test spec.
    :param status:      The return status for the command.
    :param out_data:    The data the command wrote to stdout.
    :param err_data:    The data the command wrote to stderr. Not used.

    :type config:       T
    :type status:       int
    :type out_data:     str
    :type err_data:     str

    :return:            A tuple (success, fail reason) where success is True if the
                        test passed and false otherwise and the fail reason is a
                        string explaining the test result.
    :rtype:             (bool, str)

    :raise ValueError:  If the config is malforrmed.
    """

    if isinstance(config, str):
        patterns = [config]
    elif isinstance(config, list):
        patterns = config
    else:
        raise ValueError('Bad config - expect regex or list of regex: {}'.format(config))

    # ---------------------------------------
    # Compile all the patterns to make sure they're ok
    compiled_patterns = []
    for pat in patterns:
        try:
            compiled_patterns.append(re.compile(pat))
        except Exception:
            raise ValueError('Bad config - bad regex: {}'.format(pat))

    # ---------------------------------------
    # Run the patterns over each line in the file.
    for l in out_data.splitlines():
        for pat in compiled_patterns:
            if re.search(pat, l):
                return True, '{}'.format(pat.pattern)

    return False, 'No match'
