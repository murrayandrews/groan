"""
Groan expect plugin to check if stdout or stderr or a specified file is empty.
This cannot be checked using the match plugins. i.e. '^$' does not work as it
will look for empty lines not an empty string.

Usage is:
    empty: stdout
or
    empty: stderr

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

import os

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def check(config, status, out_data, err_data):
    """
    Check that either stdout or stderr or a specified file is empty.

    :param config:      The configuration for this check read from the YAML test
                        spec. This must be a single string containing either
                        'stdout', 'stderr' or a file name.
    :param status:      The return status for the command.
    :param out_data:    The data the command wrote to stdout.
    :param err_data:    The data the command wrote to stderr.

    :type config:       T
    :type status:       int
    :type out_data:     str
    :type err_data:     str

    :return:            A tuple (success, fail reason) where success is True if
                        the test passed and false otherwise and the fail reason
                        is a string explaining the test result.
    :rtype:             (bool, str)

    :raise ValueError:  If the config is malforrmed.
    """

    if config == 'stdout':
        return len(out_data) == 0, config
    elif config == 'stderr':
        return len(err_data) == 0, config

    # Assume its a file
    try:
        st = os.stat(config)
    except OSError as e:
        return False, '{}: {}'.format(config, e.strerror)
    except Exception as e:
        return False, '{}: {}'.format(config, e)

    return st.st_size == 0, config
