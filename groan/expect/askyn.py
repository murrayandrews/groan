"""
Groan expect plugin to prompt the user to check something.

WARNING: This uses stdout and stdin.

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

__author__ = 'Murray Andrews'


# ------------------------------------------------------------------------------
def get_user_input(prompt, valid_responses, default=None, case_sensitive=True, error_msg=None):
    """
    Get responses from the user via terminal. Whitespace is trimmed from user
    responses before processing. Does not handle EOF or Interrupt at all.

    :param prompt:      String or list of strings to print as a prompt.
    :param valid_responses: A list, set or tuple of valid response strings from the user
    :param default:     If empty response from the user then uses this as the default.
                        Warning: No check made to see that the default is valid resposne.
                        If None then no default and the user must supply a response.
    :param case_sensitive: If True (default) user responses are case sensitive
    :param error_msg:   If not None then use this as an error message when invalid
                        response provided by user. If None, construct a suitable
                        message automatically.

    :return:            The user response is returned. If caseSensitive is False then
                        the response returned is always lower case - no matter what was
                        typed. Even if its the default and it was originally supplied
                        in upper case.

    """

    if not case_sensitive:
        valid = {s.lower() for s in valid_responses}
        if default is not None:
            default = default.lower()
    else:
        valid = set(valid_responses)

    if error_msg is None:
        error_msg = 'Invalid response. Must be one of ' + ', '.join(valid_responses)

    # Now get user input

    while True:

        # Print the prompt and get the input
        if isinstance(prompt, str):
            response = raw_input(prompt + ' ')
        else:
            for s in prompt[:-1]:
                print(s)

            response = raw_input(prompt[-1] + ' ')

        response = response.strip()

        if not case_sensitive:
            response = response.lower()

        if response == '' and default is not None:
            return default

        if response in valid:
            return response

        # Invalid response - print error and try again
        print(error_msg + '\n')


# ------------------------------------------------------------------------------
def ask_yn(prompt, default=None):
    """
    Ask a yes or no question and get the response. Returns either 'y' or 'n'

    :param prompt:  Prompt string for user
    :param default: Default response if user hits return. Default None.

    :return:        Either 'y' or 'n'.
    """

    yn = get_user_input(prompt, ('y', 'n', 'yes', 'no'), default, case_sensitive=False)

    return 'y' if yn[0] == 'y' else 'n'


# ------------------------------------------------------------------------------
# noinspection PyUnusedLocal
def check(config, status, out_data, err_data):
    """
    Print a prompt for the user and get a yes/no answer. Yes is treated as
    success and no as failure.

    :param config:      The configuration for this check read from the YAML test
                        spec. It must be a string used as a prompt for the user.
    :param status:      The return status for the command. Not used.
    :param out_data:    The data the command wrote to stdout. Not used.
    :param err_data:    The data the command wrote to stderr. Not used.

    :type config:       T
    :type status:       int
    :type out_data:     str
    :type err_data:     str

    :return:            A tuple (success, fail reason) where success is True if the
                        test passed and false otherwise and the fail reason is a
                        string explaining the test result.
    :rtype:             (bool, str)

    :raise ValueError:  If the config is malforrmed.
    """

    if not isinstance(config, str):
        raise ValueError('Bad config - string expected: {}'.format(config))

    result = ask_yn(config) == 'y'
    return result, '{}: {}'.format('Yes' if result else 'No', config)
