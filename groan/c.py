"""

This is a poor man's version of colorama. It uses ANSI escape sequences to
colourise strings.

A bunch of functions are created at load time to colourise strings.

The function c.x(s) will colourise a string version of object s in the
colour x on normal background.  (e.g. c.r is red, c.k is black etc.).
The full colour names can also be used. e.g. c.red().

The function c.x_y(s) will colourise s with foreground x and backgrund y.
(e.g. c.w_b() is white on black). Note that there are no c.x_x functions.
Red on red is probably not all that useful.

If the variable c.enabled is set to False then no colourising is done
but the functions can still be called - they just return the input string.
Default is True.

The variables c.fg_bright and c.bg_bright control whether bright colours
are used for the foreground and background respectively. Default for
fg_bright is False. Default for bg_bright is True.

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

from __future__ import print_function

from functools import partial
import sys

__author__ = 'Murray Andrews'

# ------------------------------------------------------------------------------
# User settable control vars.

enabled = True
fg_bright = False
bg_bright = True

# ------------------------------------------------------------------------------
# No user serviceable parts past this point.

_colours = {
    'black': 0, 'k': 0,
    'red': 1, 'r': 1,
    'green': 2, 'g': 2,
    'yellow': 3, 'y': 3,
    'blue': 4, 'b': 4,
    'magenta': 5, 'm': 5,
    'cyan': 6, 'c': 6,
    'white': 7, 'w': 7,
}

_reset = 0  # ANSI reset to normal code
_fg_offset = 30
_bg_offset = 40
_bright_offset = 60  # Add to colours to get brighter versions


# -----------------------------------------------------------------------------
def _fg_level():
    """
    Return the brightness adder for foreground colours, depending on the setting
    of the global var fg_bright.

    :return:            The brightness adder.
    :rtype:             int

    """
    return _bright_offset if fg_bright else 0


# -----------------------------------------------------------------------------
def _bg_level():
    """
    Return the brightness adder for background colours, depending on the setting
    of the global var bg_bright.

    :return:            The brightness adder.
    :rtype:             int

    """
    return _bright_offset if bg_bright else 0


# -----------------------------------------------------------------------------
def _fg(s, fg_colour=None):
    """
    Augment a string with ANSI escape sequences to render it in the specified
    foreground colour (and reset to normal at the end) if the global var
    colouring is True. Otherwise return the string itself.

    :param fg_colour:       A colour - one of the keys in c._colours.
    :param s:               An object to colourise. If this is not a
                            string then it will be converted to one .
                            with __str__().
    :type fg_colour:        str
    :type s:                T

    :return:                A (possibly) colourised string.
    :rtype:                 str
    """

    global enabled

    if enabled and fg_colour in _colours:
        return '\033[{}m{}\033[{}m'.format(_colours[fg_colour] + _fg_offset + _fg_level(),
                                           s,
                                           _reset)

    return str(s)


# -------------------------------------------------------------------------------
def _fg_on_bg(s, fg_colour=None, bg_colour=None):
    """
    Augment a string with ANSI escape sequences to render it in the specified
    foreground colour on the specified background colour (and reset to normal at
    the end) if the global var colouring is True. Otherwise return the string
    itself.

    :param fg_colour:       A colour - one of the keys in c._colours.
    :param s:               An object to colourise. If this is not a
                            string then it will be converted to one .
                            with __str__().
    :type fg_colour:        str
    :type s:                T

    :return:                A (possibly) colourised string.
    :rtype:                 str
    """

    global enabled

    if enabled and fg_colour in _colours:
        return '\033[{};{}m{}\033[{}m'.format(_colours[fg_colour] + _fg_offset + _fg_level(),
                                              _colours[bg_colour] + _bg_offset + _bg_level(),
                                              s,
                                              _reset)

    return str(s)


# ------------------------------------------------------------------------------
def _create_colour_funcs():
    """
    Create a colour function for each colours listed in _colours. So c.r()
    turns a string red.

    Also create fg on bg function for each of the combinations of fg and bg
    colours. So r_y() turns a string red on a yellow background

    """

    this_module_name = globals()['__name__']
    this_module = sys.modules[this_module_name]

    for fg in _colours:
        colour_fn = partial(_fg, fg_colour=fg)  # Not lambda - binds too late
        setattr(this_module, fg, colour_fn)

        for bg in _colours:
            if bg == fg:
                continue
            colour_fn = partial(_fg_on_bg, fg_colour=fg, bg_colour=bg)
            setattr(this_module, '{}_{}'.format(fg, bg), colour_fn)


# ------------------------------------------------------------------------------
_create_colour_funcs()


# ------------------------------------------------------------------------------
# Some tests
def demo_print():
    """
    Demo print.
    """
    # colours = ['black', 'red', 'green', 'yellow', 'blue', 'magenta', 'cyan', 'white']
    colours = [c for c in sorted(_colours) if len(c) > 1]

    this_module = sys.modules[globals()['__name__']]
    for fg in colours:
        fn = getattr(this_module, fg)
        print('--> {} <--'.format(fn(fg)))

    for fg in colours:
        for bg in colours:
            if fg == bg:
                continue
            fn = getattr(this_module, '{}_{}'.format(fg, bg))
            print('--> {} <--'.format(fn('{} on {}'.format(fg, bg))))


if __name__ == '__main__':
    fg_bright = False
    bg_bright = False
    print('Dull on Dull')
    demo_print()

    print('\nDull on Bright')
    # noinspection PyRedeclaration
    bg_bright = True
    demo_print()
