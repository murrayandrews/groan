# Welcome to Groan

Groan is a simple but extensible command line test runner.

```make
Remember: groan early and groan often!
```

It uses simple YAML configuration files to define test suites consisting of individual tests.

Each test is defined in terms of:

* A UNIX shell command that runs the test.
* A list of expected results from the test command.

## Checking Test Results

Groan uses a basic plugin architecture for checking test results. These are referred to as _expect_ plugins.
These plugins can be chained to perform multiple checks for each test.

New _expect_ plugins
can be dynamically added to the basic set. There is a standard interface between _expect_
plugins and groan itself.

Core expect plugins include:

|Plugin|Description|
|-|-|
|status|Check command exit status|
|match|Match regex patterns in the stdout from the command.|
|ematch|Match regex patterns in the stderr from the command.|
|empty|Check that stdout or stderr or a file is empty.|
|askyn|Prompt the user whether the test worked (yes/no).|

(One day the command mechanism will be plugin based too.)

## Test Parameters

Groan allows tests (commands and expected results) to be parameterised
with a simple parameter substitution mechanism that allows for:

* global parameters shared across all suites in the test run
* parameters common across a test suite
* parameters local to an individual test
* predefined parameters provided by groan itself (see below)

Parameter substitution is done as late as possible - just before the command is run or the command
results are checked. This allows test local parameters to override suite parameters which override
global parameters.

### Groan Supplied Parameters

The following parameters are supplied by groan itself.

|Parameter|Description|
|-|-|
|_suite_id|Test suite identifier|
|_suite_title|Test suite title|
|_suite_file|Name of file containing the test suite|
|_test_id|Test case identifier|
|_test_title|Test case title|


## Reuse of Test Fragments

Normal YAML reference semantics (&node / *node) can be used to define reusable
command or expect components within a suite. This can be quite powerful when combined
with parameter substitution.

## Test Selection

Each test has a test ID and may have zero or more tags. Tests can be
selected based on:

* suite (a specific YAML file containing zero or more tests)
* specified test IDs
* specified test tags.

## Sample Test Specification

The following sample runs a command that prints _No more muppets_ then checks the result.

```yaml
tests:
  - id: enomuppets
    title: 'Enough with the muppets'
    locals:
      msg: 'No more muppets'
    run: 'echo {msg}'
    expect:
      - status: 0
      - match: '{msg}'
```


## Installation

Groan requires Python 2.7 but does not require any other non-standard Python modules.

Make sure groan.py is somewhere in your PATH and is executable. e.g.:

```
#!bash

sudo install -m 0755 groan.py /usr/local/bin/groan
```

Make sure the ```groan/``` directory containing the _expect_ plugins is in your PYTHONPATH.

e.g.

```bash
PYTHONPATH=/usr/local/lib/python
export PYTHONPATH
sudo cp -r groan /usr/local/lib/python
```

or

```bash
mkdir ~/lib ~/lib/py
PYTHONPATH=~/lib/py
export PYTHONPATH
cp -r groan ~/lib/py
```

Now try:

```bash
groan --help
```

Have a look at the samples in the ```samples/``` directory

## TODO

Lots of stuff:

* Better doco
* More expect plugins
* Command plugin mechanism
* More sophisticated interface to plugins
* Better test fragment reuse
* Better variable inject and extraction from test results.