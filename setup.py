from distutils.core import setup

setup(
    name='groan',
    version='1.0',
    packages=['groan', 'groan.expect'],
    url='',
    license='Apache 2.0',
    author='Murray Andrews',
    author_email='',
    description='CLI Test Automation'
)
