#!/usr/bin/env python2

"""

Groan is an extensible test runner for shell commands. It is configured using
simple YAML files that specify the tests to run and the output expected.

Remember: Groan early and groan often.

Licensed under the terms of the Apache License, Version 2.0 (Murray Andrews 2014)

"""

from __future__ import print_function

import argparse
import sys
import yaml
import subprocess
import re
from collections import namedtuple, OrderedDict
from groan import c
import os.path

__author__ = 'Murray Andrews'

# from groan.groantypes import * ## Work in progress.

Expect = namedtuple('Expect', ['check', 'invert', 'config'])

# Signficant keys in the YAML test spec
T_ID = 'id'
T_EXPECT = 'expect'
T_RUN = 'run'
T_TAGS = 'tags'
T_TESTS = 'tests'
T_TITLE = 'title'
T_GLOBALS = 'globals'
T_LOCALS = 'locals'
T_LOAD = 'load'

# Verbosity settings
V_SILENT = 0
V_ERRORS_ONLY = 1
V_ALL_RESULTS = 2
V_ALL_OUTPUT = 3
V_DEBUG = 4

verbosity = V_ERRORS_ONLY
ofp = sys.stdout
enabled = True


# ...............................................................................
# region utils
# ...............................................................................


# ------------------------------------------------------------------------------
def import_by_name(name, parent=None):
    """
    Import a named module from within the named parent.

    :param name:            The name of the sender module.
    :param parent:          Name of parent module. Default None.

    :type name:             str
    :type parent:           str

    :return:                The sender module.
    :rtype:                 module
    """

    if parent:
        name = parent + '.' + name
    __import__(name)
    return sys.modules[name]


# ------------------------------------------------------------------------------
def param_expand(obj, *params):
    """
    Expand all {xxx} entries in any strings contained in the specified object,
    replacing them with values from the arg dictionaries.

    Only strings, lists and dicts can be expanded. Objects of any other type are
    left unchanched. For dictionaries, only values (not keys) are expanded. The
    substitutioms are done using str.format().

    :param obj:         The object to be expanded.
    :param params:      A dictionary of parameters. May be empty or None.

    :type obj:          T
    :type params:       dict[str, str]

    :return:            The expanded object
    :rtype:             T

    :raise KeyError:    If there is no xxx param to replace a {xxx} reference.
    :raise ValueError:  If an embedded format string is malformed.

    """

    if obj:
        for d in params:

            if not d:
                continue

            assert isinstance(d, dict)

            if isinstance(obj, str):
                return obj.format(**d)

            if isinstance(obj, list):
                return [param_expand(item, *params) for item in obj]

            if isinstance(obj, dict):
                for key in obj:
                    obj[key] = param_expand(obj[key], *params)

    return obj


# ------------------------------------------------------------------------------
def xor(a, b):
    """
    Compute the exclusive-or of two booleans

    :param a:       Boolean 1
    :param b:       Boolean 2
    :type a:        bool
    :type b:        bool

    :return:        Exclusive-or (xor) of a and b.
    :rtype:         bool

    :raise ValueError:
                    If either a or b are not boolean.

    """

    for arg in (a, b):
        if not isinstance(arg, bool):
            raise ValueError('{} is not bool'.format(arg))

    return a != b


# ------------------------------------------------------------------------------
def vprint(level, *args):
    """
    Print a message depending on the message level and the global verbosity
    setting.

    :param level:           The verbosity level associated with the message.
    :param args:            The message. The specified args will be joined by
                            ' ' and printed (or not) depending on the verbosity
                            level.

    :type level:            int
    :type args              str

    :return:                True if something was printed and False otherwise.
    :rtype:                 bool

    """

    global verbosity, ofp

    if level <= verbosity:
        print(' '.join(args), file=ofp)
        return True

    return False


# ------------------------------------------------------------------------------
def lists_overlap(l1, l2):
    """
    Determine if the two lists have at least one element in common. The test
    used is equality so be careful if the contents of the lists are not basic
    types.

    :param l1:      A list.
    :param l2:      A list.
    :type l1:       list[T]
    :type l2:       list[T]

    :return:        True if the lists have at least one element in common.
    :rtype:         bool
    """

    for a in l1:
        if a in l2:
            return True

    return False


# ------------------------------------------------------------------------------
def check_string_dict(d):
    """
    Check that the specified dictionary is in fact a dictionary and that all the
    keys and values are strings.

    :param d:           The dictionary.
    :type d:            dict[str, str]

    :return:            The verified dictionary.
    :rtype:             dict[str, str]

    :raise ValueError:  if d is not a dict or any key or value is not a string.

    """

    if not isinstance(d, dict):
        raise ValueError('dict expected')

    for key in d:
        if not isinstance(key, str):
            raise ValueError('key {} is not a string'.format(key))

        if not isinstance(d[key], str):
            raise ValueError('value for key {} is not a string'.format(key))

    return d


# ------------------------------------------------------------------------------
def is_list_of_type(l, t):
    """
    Check whether or not l is a list of item of type t.

    :param l:       An object to test.
    :param t:       A type
    :type l:        T
    :type t:        type
    :return:        True if l is a list of strings, False otherwise.
    :rtype:         bool
    """

    if not isinstance(l, list):
        return False

    for item in l:
        if not isinstance(item, t):
            return False

    return True


# ..............................................................................
# endregion utils
# ..............................................................................


# ------------------------------------------------------------------------------
class GroanException(Exception):
    """
    For test related exceptions.
    """
    pass


# ------------------------------------------------------------------------------
class GroanTest(object):
    """
    Class to hold the configuration for a single test.
    """

    # --------------------------------------------------------------------------
    @staticmethod
    def _validate(t):
        """
        Perform a basic check on the specified test to make sure the critical
        elements are present.

        :param t:            A test spec read from YAML file.
        :type t:             dict[str, T]

        :raise TestException:   If the test is malformed.
        """

        if not isinstance(t, dict):
            raise GroanException('Bad test spec: expected dict, got: {}'.format(t))
        for required in (T_ID, T_RUN, T_EXPECT):
            if required not in t:
                raise GroanException('Test ID {}: Missing {} element'.format(
                    t[T_ID] if T_ID in t else '??', required))

        if not isinstance(t[T_RUN], str):
            raise GroanException('Test ID {}: Run element must be a string'.format(t[T_ID]))

    # --------------------------------------------------------------------------
    # noinspection PyTypeChecker
    def __init__(self, suite, t):
        """
        Initialise the test from the structure read from the YAML file.

        :param suite:   The owning test suite.
        :param t:       The test structure.
        :type suite:    GroanSuite
        :type t:        dict[str, T]
        """

        GroanTest._validate(t)
        self.id = t[T_ID]
        self.title = t[T_TITLE] if T_TITLE in t else ''
        self.tags = t[T_TAGS] if T_TAGS in t else []
        self._run = t[T_RUN]
        self._params = None
        self.suite = suite

        # ---------------------------------------
        # Anlyse the expect clauses for the test result. Some may have a leading
        # 'not ' or '^' indicating the test result should be inverted.

        self._expect = []

        # ---------------------------------------
        # Process the expect clause. This must be a list of individual success
        # checks to perform.  Each element in the list is a dict of one or more
        # checks. The outer list enforeces an order for checking results and also
        # allows the same check type to be performed more than once with different
        # config. Within each entry though there is no guaranteed order of
        # execution and a given check type can only occur once.

        if not isinstance(t[T_EXPECT], list):
            raise GroanException('Test ID {}: {} key must be a list'.format(self.id, T_EXPECT))

        for expect_item in t[T_EXPECT]:

            if not isinstance(expect_item, dict):
                raise GroanException('Test ID {}: {} sub-element must be a dict'.format(self.id, T_EXPECT))

            for expect in expect_item:
                # expect should be the name of of a test type, possibly preceded by an inversion
                # specifier (i.e. 'not ' or '^'). The value of the expect key is the config info
                # needed to perform the test (e.g. expected values or similar).

                ex = re.match(r'^\s*(?P<invert>((not\s+)|(\^\s*)))?(?P<check>\w+)\s*$', expect)

                if not ex:
                    raise GroanException('Test ID {}: Cannot determine check type: {}'.format(self.id, expect_item))

                self._expect.append(Expect(
                    check=ex.group('check'),  # Check type
                    invert=bool(ex.group('invert')),  # Invert the result of the check
                    config=expect_item[expect]
                ))

        # ---------------------------------------
        # Store any parameters local to the test. We don't expand them with globals
        # and suite locals only when needed.

        if T_LOCALS in t:
            try:
                self._params = check_string_dict(t[T_LOCALS])
            except ValueError as e:
                raise GroanException('Test ID {}: {}: {}'.format(self.id, T_LOCALS, e))

            vprint(V_DEBUG, 'Test Locals:', str(self._params))

    # --------------------------------------------------------------------------
    def __str__(self):

        return '{}: {}'.format(self.id, self.title)

    # --------------------------------------------------------------------------
    # noinspection PyUnresolvedReferences
    def run(self, params):
        """
        Run the test and verify that the results are as expected

        :param params:      A dictionary of parameters available for substitution
                            in the command and expect configs for the test. To
                            this will be added the local params for the test
                            case itself but the params dict will not be altered.
                            Any string {xxx} in tcommand and expect configs will
                            be replaced by the value of the 'xxx' param. If
                            there is no 'xxx' param then an exception occurs.
        :type params:       dict[str, str]

        :return:            True if the test passed. False otherwise.
        :rtype:             bool
        :raise GroanException:
                        If the test cannot be run.

        """

        global verbosity, ofp

        # ---------------------------------------
        # Its run time. Time to expand all the param sets that will be used.
        # global params --> suite params --> test params.

        if self._params:
            test_params = params.copy()
            test_params.update(self._params)
        else:
            test_params = params

        # ---------------------------------------
        # Add some suite specific variables
        test_params['_test_id'] = self.id
        test_params['_test_title'] = self.title

        # ---------------------------------------
        # Expand the params dict itself. We do this alphabetically so there is
        # always a defined expansion order.
        try:
            test_params = {p: param_expand(test_params[p], test_params) for p in sorted(test_params)}
        except KeyError as e:
            raise GroanException('{}: Cannot expand parameter: No parameter {}'.format(self.id, e))
        except ValueError as e:
            raise GroanException('{}: Format error in parameter: {}'.format(self.id, e))

        vprint(V_DEBUG, 'Test params:', str(test_params))

        # ---------------------------------------
        # Prepare the command
        try:
            cmd = param_expand(self._run, test_params)
        except KeyError as e:
            raise GroanException('{}: Cannot expand run cmd: No parameter {}'.format(self.id, e))
        except ValueError as e:
            raise GroanException('{}: Format error in run cmd: {}'.format(self.id, e))

        vprint(V_DEBUG, 'Running:', cmd)

        # ---------------------------------------
        # Setup the subprocess
        try:
            proc = subprocess.Popen(cmd,
                                    stdout=subprocess.PIPE,
                                    stderr=subprocess.PIPE,
                                    close_fds=True,
                                    shell=True)
        except OSError as e:
            raise GroanException('{}: {}'.format(self.id, e))
        except Exception as e:
            raise GroanException('{}: {}'.format(self.id, e))

        # ---------------------------------------
        # Get the status and output data from the subprocess
        try:
            out_data, err_data = proc.communicate()
        except Exception as e:
            raise GroanException(str(e))

        # ---------------------------------------
        # Verify the result. This means running all of the verification
        # steps and ensuring that each one of them returns True. Verification
        # steps are handled by plugins from the package expect. A verify step of xxx
        # must have a module expect.xxx containing a function check().

        # Expand any params in the expect result
        try:
            # noinspection PyProtectedMember
            expect = [ex._replace(config=param_expand(ex.config, test_params)) for ex in self._expect]
        except KeyError as e:
            raise GroanException('{}: Cannot expand expect structure: No parameter {}'.format(self.id, e))
        except ValueError as e:
            raise GroanException('{}: Format error in expect structure: {}'.format(self.id, e))

        vprint(V_DEBUG, 'Expect:', str(expect))
        result = True
        header_printed = False

        for check in expect:
            # Try to load an expect type specific verifier. A leading 'not 'or ^ means
            # invert the success result

            vprint(V_DEBUG, 'Check:', str(check))
            try:
                checker = import_by_name(check.check, 'groan.expect')
            except ImportError:
                raise GroanException('{}: No expect plugin for "{}"'.format(self.id, check.check))

            if not hasattr(checker, 'check'):
                raise GroanException('{}: Invalid expect plugin for "{}"'.format(self.id, check.check))

            # Run the checker
            try:
                result, info = checker.check(check.config, proc.returncode, out_data, err_data)
            except ValueError as e:
                raise GroanException(str(e))

            assert isinstance(result, bool), 'Non-boolean result from expect plugin: {}'.format(check.check)

            result = xor(result, check.invert)

            # Print the data returned by the command if the test failed.
            result_data_print_level = V_ALL_OUTPUT if result else V_ERRORS_ONLY

            # Print results
            if not header_printed or not result:
                header_printed = vprint(V_ALL_RESULTS if result else V_ERRORS_ONLY, c.b('Test: ' + str(self)))
                vprint(V_ALL_RESULTS, c.m('Run:'), c.m(cmd))
                vprint(result_data_print_level, c.c(40 * '.'))
                vprint(result_data_print_level, c.c('Status:'), c.c(proc.returncode))
                vprint(result_data_print_level, c.c('.. stdout ..............................'))
                vprint(result_data_print_level, c.c(out_data))
                vprint(result_data_print_level, c.c('.. stderr ..............................'))
                vprint(result_data_print_level, c.c(err_data))
                vprint(result_data_print_level, c.c(40 * '.'))

            colour_it = c.g if result else c.r
            vprint(V_ALL_RESULTS if result else V_ERRORS_ONLY,
                   colour_it('{}: {}{}'.format('Pass' if result else 'Fail',
                                               check.check,
                                               ': ' + info if info or not result else '')))

            if not result:
                break

        if header_printed:
            vprint(V_ALL_RESULTS if result else V_ERRORS_ONLY, c.b(80 * '-'))

        return result


# ------------------------------------------------------------------------------
class GroanSuite(list):
    """
    A suite (list) of GroanTests.

    """

    # Suite IDs that have already been loaded. Keys are suite IDs, values are suites
    suites = OrderedDict()

    # Files that have already been loaded. Key is the absolute path, value is
    # name supplied by user.
    _loaded_files = dict()

    global_params = {}  # Global groan test params

    # --------------------------------------------------------------------------
    def __init__(self, sid):
        """

        :param sid:         Suite identifier.
        :type sid:          str
        """

        super(GroanSuite, self).__init__()
        self._local_params = {}  # Local params for the suite
        self._global_params = {}  # Global params defined in this suite

        self.id = sid
        self.title = None
        self._expanded_params = None
        self.loadfiles = []  # A list of other files to load for this suite.
        self.filename = None

    # --------------------------------------------------------------------------
    def __str__(self):
        return '{}: {}: {}'.format(
            self.id, self.filename, ', '.join(gtest.id for gtest in self))

    # --------------------------------------------------------------------------
    @classmethod
    def load(cls, gfiles, basedir=None, depth=0):
        """
        Load the specified groan test suites. We take care not to load the samae
        suite twice. "load" directives in a groan suite may cause other suites to
        be loaded as well. The loaded suites are stored in an OrderedDict keyed
        using the suite ID as key. This OrderedDict is accessible as the class
        variable GroanSuite.suites.

        :param gfiles:          Either a single file name or a list of file names.
        :param depth:           Call depth. Default 0
        :param basedir:         All relative filenames are treated as being
                                relative to this directory. If None then the
                                current working directory is assumed.
        :type gfiles:           list[str] | str
        :type depth:            int

        """

        if isinstance(gfiles, str):
            gfiles = [gfiles]

        indent = ' ' * 4 * depth

        for g in gfiles:

            # ---------------------------------------
            # Determine the relative and absolute path names for the file to load.

            if not os.path.isabs(g):
                if g.startswith('~'):
                    g = os.path.expanduser(g)
                elif basedir:
                    # g = os.path.normpath(os.path.join(basedir, g))
                    g = os.path.join(basedir, g)

            g_abs = os.path.abspath(g)

            # ---------------------------------------
            # Make sure we have not loaded this file before. This is not considered
            # to be an error - we just skip this file after the first load.

            if g_abs in cls._loaded_files:
                vprint(V_DEBUG, indent + 'Skipping {}: already loaded as {}'.format(g, cls._loaded_files[g_abs]))
                continue

            cls._loaded_files[g_abs] = g
            if os.path.isdir(g):
                vprint(V_ERRORS_ONLY, indent + 'Skipping: {} - cannot handle directories yet sorry'.format(g))
                continue

            # ---------------------------------------
            # Load the next suite - let exceptions bubble up
            suite = GroanSuite._load(g, depth=depth)

            # Note 'if not suite: ..' won't work here as a suite may have no tests
            # but is not empty.
            if suite is None:
                continue

            # ---------------------------------------
            # Make sure this suite ID has not been used previously.
            # This is considered to be an error.
            if suite.id in cls.suites:
                raise GroanException('Duplicate suite ID {} in files {} and {}'.format(
                    suite.id, cls.suites[suite.id].filename, g))

            # ---------------------------------------
            # Load any other files this suite has requested. These are loaded
            # before the current suite is added to the suite store which means
            # loaded files will run before the tests in the suite that requested
            # the load and any globals in the loaded suite can be overridden in
            # the suite requesting the load.

            cls.load(suite.loadfiles, basedir=os.path.dirname(g), depth=depth + 1)

            # ---------------------------------------
            # Add the current suite to the list and merge its globals into the
            # overall global parameter register.

            cls.suites[suite.id] = suite
            cls.global_params.update(suite._global_params)
            vprint(V_DEBUG, indent + 'Globals:', str(cls.global_params))

    # --------------------------------------------------------------------------
    @classmethod
    def _load(cls, yaml_file, depth=0):
        """
        Load the suite in the specified YAML file and return the suite.

        :param yaml_file:       Name of a YAML file containing one or more test specs.
        :param depth:           Loading call depth. Default 0
        :type yaml_file:        str
        :type depth:            int

        :return:                A GroanSuite. May be None if the file was empty.
        :rtype:                 GroanSuite

        :raise GroanException:  If there is a problem loading any of the tests.
        """

        indent = ' ' * 4 * depth
        vprint(V_DEBUG, indent + 'Loading:', yaml_file)
        try:
            with open(yaml_file, 'r') as yfp:
                test_specs = yaml.safe_load(yfp)
        except IOError as e:
            raise GroanException('{}: {}'.format(yaml_file, e.strerror))
        except Exception as e:
            raise GroanException(str(e))

        if not test_specs:
            return None

        id_set = set()
        suite = cls(sid=test_specs[T_ID] if T_ID in test_specs else yaml_file)
        suite.title = test_specs[T_TITLE] if T_TITLE in test_specs else ''
        suite.filename = yaml_file

        # ---------------------------------------
        # Load any global params in the suite. We don't add them to the global
        # repository here. That is done by the master loader so it can control
        # the order in which they are added.
        # held in the GroanSuite class.

        if T_GLOBALS in test_specs:
            try:
                suite._global_params = check_string_dict(test_specs[T_GLOBALS])
            except ValueError as e:
                raise GroanException('Suite {}: {}: {}'.format(suite.id, T_GLOBALS, e))

            vprint(V_DEBUG, indent + 'Suite Globals:', str(suite._global_params))

        # ---------------------------------------
        # Load local params. These belong to the suite instance.

        if T_LOCALS in test_specs:
            try:
                suite._local_params = check_string_dict(test_specs[T_LOCALS])
            except ValueError as e:
                raise GroanException('Suite {}: {}: {}'.format(suite.id, T_LOCALS, e))

            vprint(V_DEBUG, indent + 'Locals:', str(suite._local_params))

        vprint(V_DEBUG, indent + 'Loading individual tests ..')

        # ---------------------------------------
        # See if other files will need to be loaded. We don't do the actual
        # load here. The master loader does that.

        if T_LOAD in test_specs and test_specs[T_LOAD]:
            if isinstance(test_specs[T_LOAD], str):
                suite.loadfiles = [test_specs[T_LOAD]]
            elif is_list_of_type(test_specs[T_LOAD], str):
                suite.loadfiles = test_specs[T_LOAD]
            else:
                raise GroanException('{} key must be a string or list of strings'.format(T_LOAD))

        # ---------------------------------------
        # Create a GroanTest item for each test case in the suite.

        if T_TESTS in test_specs:

            for t in test_specs[T_TESTS]:
                try:
                    test = GroanTest(suite, t)
                except GroanException as e:
                    raise GroanException('{}: {}'.format(yaml_file, e))

                vprint(V_DEBUG, indent + 'Loaded Test:', str(test.id))

                # Complain about duplicate test IDs in the one file.
                if test.id in id_set:
                    raise GroanException('Duplicate test ID: {}'.format(test.id))

                id_set.add(test.id)

                suite.append(test)

        return suite

    # --------------------------------------------------------------------------
    def select(self, ids=None, tags=None):
        """
        Select those tests in the suite that match the ids and tags constraints.
        If both ids and tags are specified then a test must match both to be
        run. This method returns an iterator.

        :param ids:         An iterable that contains test IDs to be included.
                            If None or empty then include all. Default None.
        :param tags         An iterable that contains test tags to be
                            included. If None or empty then include all.
                            Default None.
        :return:            An iterator for GroanTests.
        :rtype:             collections.Iterable
        """

        for gtest in self:
            # Select the tests with specified IDs.
            if ids and gtest.id not in ids:
                continue

            # Select the tests with any of the specified tags
            if tags and not lists_overlap(gtest.tags, tags):
                continue

            yield gtest

    # --------------------------------------------------------------------------
    # noinspection PyUnresolvedReferences
    def run(self, ids=None, tags=None, abort=False):
        """
        Run all the tests in the specified test suite that match the ids and
        tags constraints. If both ids and tags are specified then a test
        must match both to be run. Prior to running the tests the global and
        local params will be merged and subsituted into {key} elements of
        commands.

        :param abort:       If True then abort when any test fails otherwise
                            continue on. Default False (continue).
        :param ids:         An iterable that contains test IDs to be included.
                            If None or empty then include all. Default None.
        :param tags         An iterable that contains test tags to be
                            included. If None or empty then include all.
                            Default None.
        :type abort:        bool

        :return:            True if all tests passed otherwise False.
        :rtype:             bool
        """

        # vprint(V_ERRORS_ONLY, 80 * '*')
        vprint(V_ERRORS_ONLY, '')
        vprint(V_ERRORS_ONLY, c.k_w('{:80s}'.format('Suite: ' + self.id + ': ' + self.title)))
        vprint(V_ERRORS_ONLY, '')
        # vprint(V_ERRORS_ONLY, 80 * '*')

        # ---------------------------------------
        # Merge the supplied params with the suite local params.
        # Don't alter the originals.
        if self._local_params:
            test_params = self.__class__.global_params.copy()
            test_params.update(self._local_params)
        else:
            test_params = self.__class__.global_params

        # ---------------------------------------
        # Add some suite specific variables
        test_params['_suite_id'] = self.id
        test_params['_suite_title'] = self.title
        test_params['_suite_file'] = self.filename

        result = True

        # ---------------------------------------
        # Run the tests

        for t in self.select(ids=ids, tags=tags):
            assert isinstance(t, GroanTest)

            result = t.run(test_params) and result
            if abort and not result:
                return False

        return result


# ------------------------------------------------------------------------------
# noinspection PyUnresolvedReferences
def main():
    """
    Main processing loop.

    """

    global verbosity, ofp

    argp = argparse.ArgumentParser(description='Run groan tests')
    argp.add_argument('-a', '--abort', action='store_true', default=False,
                      help='Abort on error. Default is to continue')
    argp.add_argument('-c', '--nocolour', action='store_true', default=False,
                      help='Disable use of ANSI colour sequences when printing results')
    argp.add_argument('-i', '--id', action='append',
                      help='Select tests with specified ID. Can be specified'
                           ' multiple times to select multiple IDs')
    argp.add_argument('-l', '--list', action='store_true',
                      help="List tests that would run but don't run them")
    argp.add_argument('-t', '--tag', action='append',
                      help='Select tests with specified tag. Can be specified'
                           ' multiple times to select multiple tags')
    argp.add_argument('-o', '--output', action='store',
                      help='Output file (default stdout)')
    argp.add_argument('-v', '--verbosity', action='store', default=verbosity,
                      type=int, choices=range(V_SILENT, V_DEBUG + 1),
                      help='Verbosity level ({} to {})'.format(V_SILENT, V_DEBUG))
    argp.add_argument('file', nargs='+', help='YAML file')

    args = argp.parse_args()

    if args.output:
        try:
            ofp = open(args.output, 'w')
        except Exception as e:
            print(e, file=sys.stderr)
            exit(1)

    c.enabled = not args.nocolour

    verbosity = args.verbosity

    # ---------------------------------------
    # Load the tests.

    try:
        GroanSuite.load(args.file)
    except GroanException as e:
        print(e, file=sys.stderr)
        exit(1)

    # ---------------------------------------
    if args.list:
        for suite in GroanSuite.suites.itervalues():
            assert isinstance(suite, GroanSuite)
            print('{}: {}'.format(suite.id, suite.filename), file=ofp)
            for t in suite.select(ids=args.id, tags=args.tag):
                print('    {}'.format(c.c(str(t))), file=ofp)
        exit(0)

    # ---------------------------------------
    # Run the test suites

    result = True
    for suite in GroanSuite.suites.itervalues():
        assert isinstance(suite, GroanSuite)
        try:
            result = suite.run(abort=args.abort, ids=args.id, tags=args.tag) and result
        except GroanException as e:
            print('{}: {}'.format(suite, e), file=sys.stderr)
            exit(1)

        if args.abort and not result:
            break

    ofp.close()
    exit(0 if result else 2)


# ------------------------------------------------------------------------------
if __name__ == '__main__':
    main()
